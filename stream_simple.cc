// system
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>
// eemagine sdk
#include <eemagine/sdk/channel.h>
// self
#include <eemagine/sdk/helper.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
//
//
//
void test_stream(const std::unique_ptr<eemagine::sdk::stream> &stream) {
  eemagine::sdk::helper::print(stream);

  // call getData 16 times in a row, with 500ms delays
  const int repeat(16);
  const int delay_ms(500);
  const auto stamp(std::chrono::steady_clock::now());
  for (int i = 0; i < repeat; ++i) {
    // stamp += std::chrono::milliseconds(delay_ms);
    std::this_thread::sleep_until(stamp +
                                  std::chrono::milliseconds(delay_ms) * i);
    auto data(stream->getData());
    eemagine::sdk::helper::print(data);
  }
}
//
//
//
void test_amplifier(
    const std::unique_ptr<eemagine::sdk::amplifier> &amplifier) {
  eemagine::sdk::helper::print(amplifier);

  // create stream and run test
  for (const auto &rate : amplifier->getSamplingRatesAvailable()) {
    std::unique_ptr<eemagine::sdk::stream> stream(
        amplifier->OpenEegStream(rate));
    std::cout << "stream with " << rate << "Hz sampling rate created\n";
    test_stream(stream);
  }
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  try {
    eemagine::sdk::helper helper;

    const auto version(helper.getVersion());
    eemagine::sdk::helper::print(version);

    // move pointers provided by factory into unique_ptr's
    std::vector<std::unique_ptr<eemagine::sdk::amplifier>> amplifiers;
    for (auto amplifier : helper.getAmplifiers()) {
      amplifiers.push_back(
          std::unique_ptr<eemagine::sdk::amplifier>(amplifier));
    }
    // for each amplifier
    for (const auto &amplifier : amplifiers) {
      test_amplifier(amplifier);
    }
  } catch (const std::exception &e) {
    std::cerr << "error: " << e.what() << "\n";
    return -1;
  }
  return 0;
}
