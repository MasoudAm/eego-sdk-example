// system
#include <iomanip>
#include <iostream>
// sdk helper
#include <eemagine/sdk/helper.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
template <typename Container>
void print_container(const Container &c, size_t rows,
                     const std::string &prefix) {
  const std::string indent(prefix.size(), ' ');
  std::cout << prefix;
  size_t n(0);
  for (const auto &i : c) {
    std::cout << " " << i;
    ++n;
    if (n == rows) {
      std::cout << "\n" << indent;
      n = 0;
    }
  }
  std::cout << "\n";
}
} // namespace
///////////////////////////////////////////////////////////////////////////////
eemagine::sdk::helper::helper()
#if defined(EEGO_SDK_BIND_DYNAMIC)
#ifdef _WIN32
    : _factory("eego-SDK.dll")
#endif
#ifdef __unix__
    : _factory("libeego-SDK.so")
#endif
#endif
{
}
///////////////////////////////////////////////////////////////////////////////
void eemagine::sdk::helper::print(
    const eemagine::sdk::factory::version &version) {
  std::cout << "sdk version: " << version.major << "." << version.minor << "."
            << version.micro << "." << version.build << "\n";
}
///////////////////////////////////////////////////////////////////////////////
void eemagine::sdk::helper::print(const eemagine::sdk::amplifier *amplifier) {
  std::cout << "amplifier:\n";
  std::cout << "  type...... " << amplifier->getType() << "\n";
  std::cout << "  version... " << amplifier->getFirmwareVersion() << "\n";
  std::cout << "  serial.... " << amplifier->getSerialNumber() << "\n";
  print_container(amplifier->getChannelList(), 4, "  channels..");
  print_container(amplifier->getSamplingRatesAvailable(), 10, "  rates.....");
  print_container(amplifier->getReferenceRangesAvailable(), 10, "  ref ranges");
  print_container(amplifier->getBipolarRangesAvailable(), 10, "  bip ranges");
}
///////////////////////////////////////////////////////////////////////////////
void eemagine::sdk::helper::print(const eemagine::sdk::stream *stream) {
  std::cout << "stream:\n";
  print_container(stream->getChannelList(), 4, "  channels..");
}
///////////////////////////////////////////////////////////////////////////////
void eemagine::sdk::helper::print(const eemagine::sdk::buffer &data) {
  std::cout << "data: " << std::setw(4) << data.getChannelCount() << "x"
            << std::setw(4) << data.getSampleCount();

  // show first 8 values of first sample
  int max_channel(data.getChannelCount() < 10 ? data.getChannelCount() : 10);
  int max_sample(data.getSampleCount() < 1 ? data.getSampleCount() : 1);
  for (int channel_id = 0; channel_id < max_channel; ++channel_id) {
    for (int sample_id = 0; sample_id < max_sample; ++sample_id) {
      std::cout << " | " << std::setprecision(5) << std::setw(10)
                << data.getSample(channel_id, sample_id);
    }
  }
  std::cout << " |\n";
}
